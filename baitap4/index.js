// console.log("Yes");

function tinh() {

var chieuDai=document.getElementById("txt-dai").value*1;
var chieuRong=document.getElementById("txt-rong").value*1;

var chuVi = (chieuDai + chieuRong) * 2;
var dienTich = chieuDai * chieuRong;

document.getElementById("chuVi").innerHTML = `Chu Vi là: ${chuVi}`;
document.getElementById("dienTich").innerHTML =`Diện Tích là: ${dienTich}`;



}

/**
 * Input:
 * Chiều dài HCN: người dùng nhập
 * Chiều rộng HCN: người dùng nhập
 * 
 * 
 * Todo:
 * S1: Tạo 2 biến lưu trong input: Chiều dài HCN: người dùng nhập
 *                                 Chiều rộng HCN: người dùng nhập
 * 
 * S2: Tạo 2 biến lưu trong output: chuvi = (dài + rộng) * 2;
 *                                  dientich = dài * rộng;
 *              
 * S3: Tính và xuất ra diện tích, chu vi của HCN đó ta dùng công thức: chuvi = (dài + rộng) * 2
 *                                                                     dientich = dài * rộng
 * 
 * Output:    document.getElementById("chuvi").innerHTML=`Chu vi của hình chữ nhật là <span> ${chuvi}</span>`
 *            document.getElementById("dientich").innerHTML=`Dien tich của hình chữ nhật là <span> ${dientich}</span>`
 *  
 * 
 */