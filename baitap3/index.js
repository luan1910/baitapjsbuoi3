// console.log("Yes");

function quydoi() {

var soTiendoi=document.getElementById("txt-usd").value*1;
var usd = 23500;

result = 0;

result = soTiendoi * usd;

document.getElementById("result").innerText = `Tổng Số Tiền Đổi Sang VND: ${result}VND`;


}

/**
 * Input:
 * Tỷ Giá USD: 23.500
 * Sô tiền USD muốn đổi: ( cho người dùng nhập: 1, 2, 3, 4,)
 * 
 * 
 * Todo:
 * S1: Tạo 2 biến lưu trong input:  Tỷ Giá USD: 23.500
 *                                  Sô tiền USD muốn đổi: ( cho người dùng nhập: 1, 2, 3, 4,)
 * 
 * S2: Tạo 1 biến lưu trong output: 
 * 
 * S3: Tính và xuất ra số tiền quy đổi VND dùng công thức: ( Tỷ giá USD * Số tiền USD muốn đổi)
 * 
 * 
 * Output:
 * Result: .....  document.getElementById("result").innerHTML=`Số tiền quy đổi sang VND là: <span> ${result}</span> VND`
 * 
 *                Ví Dụ: Tỷ Giá USD: 23.500
 *             Số tiền USD muốn đổi: 4
 *             Số tiền đổi từ USD sang VND: 94.000 VND
 */