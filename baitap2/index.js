// console.log("Yes");
function tinhTrungbinh() {

var num1=document.getElementById("txt-1").value*1;
var num2=document.getElementById("txt-2").value*1;
var num3=document.getElementById("txt-3").value*1;
var num4=document.getElementById("txt-4").value*1;
var num5=document.getElementById("txt-5").value*1;

var result = 0;

result = (num1 + num2 + num3 + num4 + num5) / 5;

document.getElementById("result").innerText = `Giá Trị Trung Bình Là: ${result}`;

}


/**
 * Input:
 * number1: 10
 * number2: 20
 * number3: 30
 * number4: 40
 * number5: 50
 * 
 * 
 * Todo:
 * S1: tạo 5 biến lưu trong input: number1,number2,number3,number4,number5
 * S2: tạo 1 biến lưu trong output
 * 
 * S3: Tính giá trị trung bình ta sử dụng công thức: (number1,number2,number3,number4,number5 / 5)
 * 
 * Output:
 * Result: .....Ví Dụ: (10, 20, 30, 40, 50 / 5)
 *              Kết quả: ( 30 )
 */           